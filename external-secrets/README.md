# Sécurisation des secrets

Pour sécuriser nos secrets, nous allons utiliser [`external-secrets`](https://external-secrets.io/) pour simplifier la délégation de la gestion des secrets.

## Installation

`external-secrets` fournit un Helm chart pour une installation simple

On ajoute donc le repo pour `external-secrets` :

```bash
helm repo add external-secrets https://charts.external-secrets.io
helm repo update
```

et on installe dans un namespace dédié 🚀

```bash
helm install external-secrets external-secrets/external-secrets -n external-secrets --create-namespace --set installCRDs=true
```

!!! success "external-secrets est installé"
    ```console
    external-secrets has been deployed successfully!
    ```

`external-secrets` instancie plusieurs composants, on peut les lister:

```bash
kubectl get all -n external-secrets
```

```console
NAME                                                   READY   STATUS    RESTARTS   AGE
pod/external-secrets-5f45b6f844-27fmq                  1/1     Running   0          2m9s
pod/external-secrets-cert-controller-9795887f6-8mssr   1/1     Running   0          2m9s
pod/external-secrets-webhook-6f4789ccf-qmpkn           1/1     Running   0          2m9s

NAME                               TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)   AGE
service/external-secrets-webhook   ClusterIP   10.3.175.44   <none>        443/TCP   2m11s

NAME                                               READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/external-secrets                   1/1     1            1           2m10s
deployment.apps/external-secrets-cert-controller   1/1     1            1           2m10s
deployment.apps/external-secrets-webhook           1/1     1            1           2m10s

NAME                                                         DESIRED   CURRENT   READY   AGE
replicaset.apps/external-secrets-5f45b6f844                  1         1         1       2m9s
replicaset.apps/external-secrets-cert-controller-9795887f6   1         1         1       2m9s
replicaset.apps/external-secrets-webhook-6f4789ccf           1         1         1       2m9s
```

Tout est prêt !

## Configuration

Pour permettre à `external-secrets` de gérer les secrets, il est nécessaire de créer un `SecretStore`. Celui-ci fera le lien entre le cluster et l'outil hébergé les secrets.

Pour faire simple dans ce workshop, on va faire avec GitLab pour stocker nos données sensibles via l'utilisation des variables de CICD.
C'est simple à mettre en oeuvre mais introduit une faiblesse coté sécurité car nécessite quand même d'avoir un secret coté Kubernetes, pour stocker la clé d'API pour se connecter à GitLab.

??? example "D'autres solutions sont possibles..."
    `external-secrets` fournit plusieurs [connecteurs](https://external-secrets.io/latest/provider/) possibles. Le plus complet semble être celui d'HashiCorp Vault car il n'introduit pas de besoin de clé ou de credentials pour s'intégrer dans le cluster.

On a déjà préparer les variables dans le projet GitLab :

![img.png](secrets.png)


### Créons le secret de connexion:

```bash
kubectl -n external-secrets create secret generic gitlab-token --from-literal=token=$GITLAB_TOKEN
```

!!! tip "GITLAB_TOKEN déjà configuré pour vous"
    On est sympa, c'est déjà fait grâce au script exécuté au début du workshop.
    Si vous avez changer de terminal, il faut refaire la commande suivante:
    ```bash
    source <(curl -s https://heracles.yodamad.fr/setup/snowcamp-2024/setup.sh)
    ```

### Maintenant on peut créer un `ClusterSecretStore` pour faire le lien entre GitLab et le cluster.

```yaml
apiVersion: external-secrets.io/v1beta1
kind: ClusterSecretStore
metadata:
  name: gitlab-cluster-secret-store
  namespace: external-secrets
spec:
  provider:
    # provider type: gitlab
    gitlab: # (1)
      url: https://gitlab.com/ # (2)
      auth:
        SecretRef:
          accessToken:
            name: gitlab-token
            key: token
            namespace: external-secrets
      projectID: "53147568" # (3)
```

1. 👽 Le type du provider
2. 🔗 L'URL de votre GitLab, pas forcément gitlab.com
3. 📦 L'ID du projet qui héberge les données sensibles

??? info
    Il est possible d'avoir des `SecretStore` restreint à un namespace si vous voulez limiter l'accessibilité à vos secrets.

On peut déployer notre `ClusterSecretStore`:

```bash
kubectl apply -f external-secrets/external-secrets-secret-store.yml -n external-secrets
```
et vérifier que le connexion à GitLab est opérationnelle:

```bash
kubectl get clustersecretstores.external-secrets.io -n external-secrets
```

!!! success "ClusterSecretStore est valide"
    ```console
    NAME                          AGE     STATUS   CAPABILITIES   READY
    gitlab-cluster-secret-store   2m50s   Valid    ReadOnly       True
    ```

## Configuration d'un `ExternalSecret` 

Nos secrets stockés dans GitLab, il faut créer désormais créer un `ExternalSecret` qui va rappatrié dans notre cluster le secret stocké dans GitLab.

On va faire le test sur `external-dns` pour externaliser notre clé d'API et username Cloudflare dans GitLab:

```bash
kubectl apply -n external-dns -f external-secrets/external-secrets-external-secret.yml
```

```console
externalsecret.external-secrets.io/external-secret-cloudflare-key-credentials created
externalsecret.external-secrets.io/external-secret-cloudflare-mail-credentials created
```

On peut vérifier que nos secrets sont créés et valides:

```bash
kubectl get externalsecrets.external-secrets.io -n external-dns
```

!!! success
    ```console
    NAME                                          STORE                         REFRESH INTERVAL   STATUS         READY
    external-secret-cloudflare-key-credentials    gitlab-cluster-secret-store   1h                 SecretSynced   True
    external-secret-cloudflare-mail-credentials   gitlab-cluster-secret-store   1h                 SecretSynced   True
    ```

Automatiquement `external-secrets` a généré des secrets pour nous:

```bash
kubectl get secrets -n external-dns
```

!!! success "Les nouveaux secrets sont générés"
    ```console
    NAME                            TYPE     DATA   AGE
    cloudflare-api-token            Opaque   1      4h53m
    cloudflare-user-mail            Opaque   1      15h
    external-cloudflare-api-token   Opaque   1      99s
    external-cloudflare-user-mail   Opaque   1      98s
    ```

## Configuration de `external-dns` pour utiliser les nouveaux secrets

Maintenant que l'on a nos nouveaux secrets, on peut mettre à jour notre `Deployment` d'`external-dns` pour qu'il les utilise plutôt que les anciens.

On change donc la référence des secrets dans le `Deployment`:

```yaml
- name: CF_API_TOKEN
  valueFrom:
    secretKeyRef:
      name: external-cloudflare-api-token
      key: api-key
- name: CF_API_EMAIL
  valueFrom:
    secretKeyRef:
      name: external-cloudflare-user-mail
      key: user-mail
```

On aura plus besoin de nos anciens secrets, autant les supprimer:

```bash
kubectl delete secret cloudflare-api-token cloudflare-user-mail -n external-dns
```

Et on met à jour `external-dns` pour utiliser les nouveaux

```bash
kubectl apply -f external-secrets/external-dns-cloudflare.yml -n external-dns
```

Après quelques secondes, on voit qu'un nouveau pod a été créé et fonctionne:

```bash
kubectl get po -n external-dns
```

!!! success "external-dns est recréé et opérationnel"
    ```
    NAME                                  READY   STATUS    RESTARTS   AGE
    external-dns-XXX-7d4cf677f6-nmwn5     1/1     Running   0          25s
    ```

et on vérifie que la connexion avec Cloudflare est toujours ok:

```bash
kubectl logs -f $(kubectl get pods -l "app.kubernetes.io/name=external-dns" -n external-dns | grep external-dns-cloudflare | cut -d' ' -f1) -n external-dns
```

!!! success "external-dns est bien connecté à Cloudflare"
    ```console
    time="2023-12-20T13:48:56Z" level=info msg="Instantiating new Kubernetes client"
    time="2023-12-20T13:48:56Z" level=info msg="Using inCluster-config based on serviceaccount-token"
    time="2023-12-20T13:48:56Z" level=info msg="Created Kubernetes client https://10.3.0.1:443"
    ```

Le chef de brigade sécurité est content, mais il sent qu'on a du répondant 😎, il nous impose alors encore de nouvelles contraintes : il ne vaut que des ingrédients contrôllés en amont et veut favoriser la filière locale plutot que la grande distribution... 🛒

Ok, challenge accepted !! 💪 Montrons-lui comment faire [➡️](../kyverno/README.md)
