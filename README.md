# 🍳 Un cluster k8s aux petits oignons 🧅

Bienvenue dans ce merveilleux cours de *cuisine* de Kubernetes.

L'objectif est de vous faire créer et paramétrer un cluster Kubernetes *from scratch* pour avoir des environnements de développements, de tests aux petits oignons pour vous et vos équipes/collègues.

## C'est parti 🍝

Avant tout, il faut cloner le repo en local dans votre répertoire préféré:

```bash
git clone https://gitlab.com/yodamad-workshops/kub-workshop-snowcamp-2024.git
```

ou ouvrir le workspace [Gitpod](https://gitpod.io/?autostart=true#https://gitlab.com/yodamad-workshops/kub-workshop-snowcamp-2024)

## Prérequis 🛠️

Pour ce workshop, vous aurez besoin des outils. Si vous avez choisi l'option `Gitpod`, ils sont déjà installés 😉

- git : [Installation](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- kubectl :  [Installation](https://kubernetes.io/fr/docs/tasks/tools/install-kubectl/)
- helm : [Installation](https://helm.sh/docs/intro/install/)
- curl : [Installation](https://everything.curl.dev/get)

Pour vérifier que tout est ok et initialiser les variables d'environnement qui vont bien, nous avons prévu un petit script (à faire aussi sur Gitpod)

```bash
source <(curl -s -u "snowcamp2024:MOT_DE_PASSE_QUON_VOUS_DONNERA" https://heracles.yodamad.fr/setup/snowcamp-2024/setup.sh)
```

Si tout se déroule comme prévu, vous devez avoir un résultat comme suit (au delta de la mise en forme suivant votre shell)

```bash
************************************************
*    👋 Bienvenue à notre super workshop 👋    *
*  Quelques vérifications avant de commencer   *
************************************************

🛂 Check local env
	🥌 curl                          ... ✅
	☸️  kubectl                       ... ✅
	🚚 helm                          ... ✅
	💻 git                           ... ✅

🛠️  Setup local env...
	🌤️  OVH connection setup          ... ✅
	🌍 Cloudfare setup                ... ✅
	🦊 GitLab setup                   ... ✅

************************************************
*              🫡  All good !!                 *
*       C'est parti, amusez vous bien 🥳       *
************************************************
```

🛫 Let's go ! Première étape : créer notre cluster [➡️](terraform/README.md)
