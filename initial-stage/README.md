# Un premier déploiement dans notre cluster

Et si on essayait de déployer un *truc* dans notre cluster, par exemple un [cinema en ascii](https://github.com/Angatar/asciinematic) 🍿

!!! note
    Rebasculer à la racine du repo `cd ..`

```bash
kubectl apply -f demos/simple-deployment.yml
```

Output:
```console
namespace/demos created
deployment.apps/simple-deployment created
service/simple-deployment-service created
```

On vérifie que tout est ok:

```bash
kubectl get deployments -n demos
```

!!! success "Démo déployée"
    ```console
    NAME                READY   UP-TO-DATE   AVAILABLE   AGE
    simple-deployment   1/1     1            1           39s
    ```

Mainteant, accédons à notre cinéma. Pour cela, il faut créer un *tunnel* vers notre cluster:

```bash
kubectl -n demos port-forward $(kubectl get pods -o=name -n demos) 8080:80
```

🎥 Notre application est disponible [http://localhost:8080](http://localhost:8080)

Ok, ca marche mais c'est pas super pratique pour:

  * travailler en équipe
  * il faut faire le mapping des ports et des hosts pour chaque application
  * ...

Pour accéder depuis l'extérieur, il va nous falloir un soupçon d'**Ingress** et un **Ingress Controller** pour gérer cela pour nous.

🛣️ En route pour la découverte de comment Nginx controller va nous faciliter la vie [➡️](../nginx-ingress-controller/README.md)
